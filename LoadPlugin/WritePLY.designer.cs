﻿namespace Plugin
{
    partial class WritePLY
	{
		/// <summary>
		/// 必要なデザイナ変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose( );
			}
			base.Dispose( disposing );
		}

		#region Windows フォーム デザイナで生成されたコード

		/// <summary>
		/// デザイナ サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディタで変更しないでください。
		/// </summary>
		private void InitializeComponent( )
		{
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.cancel = new System.Windows.Forms.Button();
            this.ok = new System.Windows.Forms.Button();
            this.saveFileDialog.Filter = "PLY files (*.ply)|*.ply|All files(*.*)|*.*";
            this.saveFileDialog.FilterIndex = 1;
            this.saveFileDialog.RestoreDirectory = true;
            this.SuspendLayout();
            // 
            // cancel
            // 
            this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancel.Location = new System.Drawing.Point(83, 111);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(68, 24);
            this.cancel.TabIndex = 8;
            this.cancel.Text = "キャンセル";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.OnCancel);
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(9, 111);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(68, 24);
            this.ok.TabIndex = 7;
            this.ok.Text = "OK";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.OnOK);
            // 
            // WritePLY
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(160, 143);
            this.ControlBox = false;
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.ok);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "WritePLY";
            this.ShowInTaskbar = false;
            this.Text = " 画像パラメータ";
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button ok;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
	}
}