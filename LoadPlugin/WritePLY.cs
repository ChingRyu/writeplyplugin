﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Reflection;

using Pluto;

namespace Plugin
{
	[PluginType( PluginCategory.Save )]
	public partial class WritePLY : Form, ICommonPlugin
	{
		#region コンストラクタ
		public WritePLY( )
		{
			InitializeComponent( );
		}
		#endregion

		#region フィールド
		DataManager dataManager = null;
		IPluto iPluto = null;
		#endregion

		#region IPlugin メンバ
		string IPlugin.Author { get { return ( "PLUTO Development Team" ); } }

        string IPlugin.Text { get { return ("Save as Polygon File"); } }

        string IPlugin.Comment { get { return ("Save mask data to PLY file"); } }
		
		bool IPlugin.Initialize( DataManager data, IPluto pluto )
		{
			// DataManager および IPluto を取得する．
			dataManager = data;
			iPluto = pluto;

			return ( true );
		}
		#endregion

		#region ICommonPlugin メンバ
		object ICommonPlugin.Run( params object[] args )
		{
			// DataManager および IPluto の取得に失敗している．または，画像が選択されていない．
			if( dataManager == null || iPluto == null || dataManager.Active == null)
			{
				return ( null );
			}

			try
			{
				// 引数の形式が一致するメソッドを呼び出す．
				return ( GetType( ).InvokeMember( "BindRun", BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod, null, this, args ) );
			}
			catch( MissingMethodException )
			{
				// 引数の形式が一致するメソッドが無い場合はデフォルトのメソッドを呼び出す．
				return ( BindRun( ) );
			}
			catch( Exception err )
			{
				// それ以外の例外はエラー．
				Console.WriteLine( err.Message );
				throw ( err );
			}
		}
		#endregion

		#region イベント
		private void OnOK( object sender, EventArgs e )
		{
			this.DialogResult = DialogResult.OK;
		}

		private void OnCancel( object sender, EventArgs e )
		{
			this.DialogResult = DialogResult.Cancel;
		}
		#endregion

		#region メソッド
		Mist.MistArray BindRun( )
		{
			// ファイル選択ダイアログ
			if( saveFileDialog.ShowDialog( ) == DialogResult.OK )
			{
				string filename = saveFileDialog.FileName;

				// 画像パラメータ設定ダイアログ
//				if( this.ShowDialog( ) == DialogResult.OK )
//				{
                    bool ret = CppRun(dataManager.Active.Image, filename);

                    if (ret)
                        dataManager.Active["PATH"] = filename;
//				}
			}

			return ( null );
		}

/*		Mist.MistArray BindRun( string filename, int w, int h, int d )
		{
			// メモリを確保する
			Mist.MistArray image = new Mist.MistArray( w, h, d );

			// 画像を読み込む
			bool ret = CppRun( image.ImagePointer, filename, w, h, d );

			if( ret )
			{
				// ファイル名を念のため保存する
				image[ "PATH" ] = filename;

				// PLUTO の管理下にデータを追加し，アクティブに設定する
				if( dataManager.Add( image, true, false ) < 0 )
				{
					// メモリを開放する
					image.Dispose( );
					return( null );
				}
			}
			else
			{
				// メモリを開放する
				image.Dispose( );
				return ( null );
			}

			return ( image );
		}*/
		#endregion

        [DllImport( "WritePLYfile.dll", EntryPoint="Run" )]
		internal static extern bool CppRun( IntPtr pImage, string filename);
	}
}