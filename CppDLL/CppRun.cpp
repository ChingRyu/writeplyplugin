#include "mist/mist.h"
#include <math.h>
#include "ply.h"
#include <vector>
#include "mist/filter/morphology.h"
#include "mist/operator/operators.h"

extern "C"
{
	__declspec( dllexport ) bool Run( mist::array3<short>* image, char* filename );
}

typedef struct Vertex {
	float x, y, z;
} Vertex;


//list of property information for a vertex
PlyProperty vert_props[] = {
		{ "x", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, x), 0, 0, 0, 0 },
		{ "y", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, y), 0, 0, 0, 0 },
		{ "z", PLY_FLOAT, PLY_FLOAT, offsetof(Vertex, z), 0, 0, 0, 0 },
};

bool Run( mist::array3<short>* image, char* filename )
{
	size_t count(0);
	Vertex v = { 0, 0, 0 };
	std::vector<Vertex> verts;

	mist::array3<short> dilationImage(*image);
	mist::dilation(dilationImage, 1);
	auto edge = dilationImage - *image;

	for (size_t z = 0; z < edge.size3(); ++z)
	{
		for (size_t y = 0; y < edge.size2(); ++y)
		{
			for (size_t x = 0; x < edge.size1(); ++x)
			{
				if (edge(x, y, z) != 0)
				{
					v.x = x * edge.reso1();
					v.y = y * edge.reso2();
					v.z = z * edge.reso3();
					verts.push_back(v);
					++count;
				}
			}
		}
	}

	int i, j;
	PlyFile *ply;
	int nelems;
	char **elist;
	int file_type;
	float version;
	int nverts = count;
//	int nfaces = sizeof(faces) / sizeof(Face);
	char* elem_name[] = { "vertex" };

	/* create the vertex index lists for the faces
	for (i = 0; i < nfaces; i++)
		faces[i].verts = vert_ptrs[i];*/

	/* open either a binary or ascii PLY file for writing */
	/* (the file will be called "test.ply" because the routines */
	/*  enforce the .ply filename extension) */

#if 1
	ply = ply_open_for_writing(filename, 1, elem_name, PLY_ASCII, &version);
#else
	ply = ply_open_for_writing("test", 2, elem_name, PLY_BINARY_BE, &version);
#endif

	/* describe what properties go into the vertex and face elements */

	ply_element_count(ply, "vertex", nverts);
	ply_describe_property(ply, "vertex", &vert_props[0]);
	ply_describe_property(ply, "vertex", &vert_props[1]);
	ply_describe_property(ply, "vertex", &vert_props[2]);

/*	ply_element_count(ply, "face", nfaces);
	ply_describe_property(ply, "face", &face_props[0]);
	ply_describe_property(ply, "face", &face_props[1]);*/

	/* write a comment and an object information field */
	ply_put_comment(ply, "author: Chenglong Wang");
	ply_put_obj_info(ply, "random information");

	/* we have described exactly what we will put in the file, so */
	/* we are now done with the header info */
	ply_header_complete(ply);

	/* set up and write the vertex elements */
	ply_put_element_setup(ply, "vertex");
	for (i = 0; i < nverts; i++)
		ply_put_element(ply, (void *)&(verts.data()[i]));

	/* set up and write the face elements
	ply_put_element_setup(ply, "face");
	for (i = 0; i < nfaces; i++)
		ply_put_element(ply, (void *)&faces[i]);*/

	/* close the PLY file */
	ply_close(ply);

	return( true );
}